/* tslint:disable:max-line-length */
import {FuseNavigationItem} from '@fuse/components/navigation';

export const defaultNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:user-group',
        link: '/example'
    }
];
export const compactNavigation: FuseNavigationItem[] = [
    {
        id: 'pacientes',
        title: 'Pacientes',
        type: 'basic',
        icon: 'heroicons_outline:user-group',
        link: '/'
    },
    {
        id: 'ajuste',
        title: 'Ajustes',
        type: 'basic',
        icon: 'heroicons_outline:cog',
        link: '/ajustes'
    }
];
export const futuristicNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:user-group',
        link: '/example'
    }
];
export const horizontalNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:user-group',
        link: '/example'
    }
];
